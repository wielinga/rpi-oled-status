SERVICE_FILE=oled-status.service
SCRIPT=sys_info_extended.py
PREFIX=/usr

install_deps:
	apt update
	apt install python3-psutil python3-luma.oled

emable_i2c:
	raspi-config nonint do_i2c 0
# Yes, 0 means enable for some reason.

install_service:
	cp $(SERVICE_FILE) /lib/systemd/system/
	systemctl daemon-reload

uninstall_service:
	systemctl stop $(SERVICE_FILE)
	rm /lib/systemd/system/$(SERVICE_FILE)
	sudo systemctl daemon-reload

install: install_deps emable_i2c install_service
	cp $(SCRIPT) $(PREFIX)/bin/$(SCRIPT)
	chmod +x $(PREFIX)/bin/$(SCRIPT)

uninstall: uninstall_service
	rm $(PREFIX)/bin/$(SCRIPT)
