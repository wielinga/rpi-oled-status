# RPi OLED Status
Daemon to show the system status of a Raspberry Pi on an OLED display. Specifically, an SSD1306 OLED.

## Installation
Simply run `sudo make install`. This should install the dependencies, enable I<sup>2</sup>C, install the script, and install the systemd service file.

Then run `sudo systemctl enable --now oled-status`.
